import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import EditAd from './pages/EditAd'
import CreateAd from './pages/CreateAd'
import ListAds from './pages/ListAds'
import Header from './components/Header'

function App() {
	return (
		<Router>
			<Header />
			<Switch>
				<Route path="/edit-ad/:id">
					<EditAd />
				</Route>
				<Route path="/create-add">
					<CreateAd />
				</Route>
				<Route exact path="/" children={<ListAds />} />
				<Route path="*">"Страницы не существует"</Route>
			</Switch>
		</Router>
	)
}

export default App
