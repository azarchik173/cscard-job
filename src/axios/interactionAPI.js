import axios from 'axios'

export const getAds = (url) => axios.get(url)

export const updateAd = (url, id, dataUpdate) =>
	axios.put(
		`${url}/${id}`,
		{ ...dataUpdate },
		{ 'Content-Type': 'application/json' }
	)

export const deleteAd = (url, id) => axios.delete(`${url}/${id}`)

export const createAd = (url, adData) =>
	axios.post(url, { ...adData }, { 'Content-Type': 'application/json' })
