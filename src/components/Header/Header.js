import React from 'react'
import styled from 'styled-components'

const HeaderContainer = styled.header`
	display: flex;
	justify-content: center;
	align-items: center;
	width: 100vw;
	height: 40px;
	box-shadow: 1px 0px 5px 0px;
	border-bottom-right-radius: 3px;
	border-bottom-left-radius: 3px;
	margin-bottom: 20px;
	font-weight: 600;
`

export const Header = () => {
	return (
		<HeaderContainer>Тестовое задание для компании CS-Card</HeaderContainer>
	)
}
