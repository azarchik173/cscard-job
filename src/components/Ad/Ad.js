import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'
import { Card, Button } from 'antd'
import { DeleteFilled, EditFilled } from '@ant-design/icons'

const { Meta } = Card

const AdContainer = styled.article`
	display: flex;
	justify-content: center;
	flex-direction: column;
	padding: 5px 0;
	margin-top: 10px;
`

export const Ad = ({
	id,
	title = 'Название отсутствует',
	description = 'Описание отсутствует',
	image,
	onDelete,
}) => {
	const [deleteLoading, updateDelete] = useState(false)
	const history = useHistory()
	return (
		<AdContainer>
			<Card
				cover={<img alt="Изображение не удалось загрузить" src={image} />}
				actions={[
					<Button
						onClick={() => history.push(`/edit-ad/${id}`)}
						type="primary"
						icon={<EditFilled />}
					>
						Редактировать
					</Button>,
					<Button
						loading={deleteLoading}
						onClick={() => {
							updateDelete(true)
							onDelete(id)
						}}
						type="primary"
						icon={<DeleteFilled />}
					>
						Удалить
					</Button>,
				]}
			>
				<Meta title={title} description={description} />
			</Card>
		</AdContainer>
	)
}

Ad.propTypes = {
	id: PropTypes.string.isRequired,
	title: PropTypes.string,
	description: PropTypes.string,
	image: PropTypes.string.isRequired,
	onDelete: PropTypes.func.isRequired,
}
