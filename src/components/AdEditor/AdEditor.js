import React, { useState } from 'react'
import { Card, Form, Input, message, Button } from 'antd'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledButton = styled(Button)`
	float: right;
`
const StyledCard = styled(Card)`
	cursor: default;
	.ant-card-body {
		padding-bottom: 0px !important;
	}
`
const StyledItem = styled(Form.Item)`
	margin-bottom: 0px !important;
	width: 100%;
	border-top: 1px solid rgba(0, 0, 0, 0.13);
	padding: 9px 0 13px 0;
`

export const AdEditor = ({
	initialData = {
		title: '',
		descrition: '',
		image: '',
	},
	title,
	onFinish,
	finishLoading,
	buttonTitle,
}) => {
	const [loadingImage, updateLoading] = useState({
		loading: false,
		loaded: false,
	})
	const [imageURL, updateURL] = useState(initialData.image)
	return (
		<StyledCard hoverable>
			<Card.Meta
				title={title}
				description={
					<Form
						initialValues={{
							title: initialData.title,
							description: initialData.descrition,
							image: initialData.image,
						}}
						validateMessages={{
							required: 'Данное поле является обязательным',
						}}
						onFinish={(e) => onFinish(e)}
					>
						<Form.Item
							name="title"
							label="Название:"
							rules={[{ required: true }]}
						>
							<Input />
						</Form.Item>
						<Form.Item
							name="description"
							label="Описание:"
							rules={[{ required: true }]}
						>
							<Input.TextArea autoSize={{ maxRows: 6 }} />
						</Form.Item>
						<Form.Item
							name="image"
							label="Изображение"
							rules={[{ required: true }]}
						>
							<Input
								onChange={(e) => updateURL(e.target.value)}
								value={imageURL}
								placeholder="Введите URL изображения"
							/>
						</Form.Item>
						<Form.Item>
							<StyledButton
								onClick={() => {
									if (imageURL.length === 0) {
										return message.warn(
											'Поле с URL изображения должно быть заполнено'
										)
									}
									updateLoading((prevState) => ({
										...prevState,
										loading: true,
									}))
									let img = new Image()
									let timeoutLoading = null
									img.src = imageURL
									img.onload = () => {
										updateLoading((prevState) => ({
											...prevState,
											loading: false,
											loaded: true,
										}))
										clearTimeout(timeoutLoading)
									}
									timeoutLoading = setTimeout(() => {
										updateLoading((prevState) => ({
											...prevState,
											loading: false,
											loaded: false,
										}))
										message.error('Не удалось загрузить изображение')
									}, 8000)
								}}
								loading={loadingImage.loading}
								type="primary"
							>
								Проверить изображение
							</StyledButton>
						</Form.Item>
						{loadingImage.loaded && (
							<Form.Item>
								<img alt="Не удалось загрузить картинку" src={imageURL} />
							</Form.Item>
						)}
						<StyledItem>
							<Button loading={finishLoading} type="primary" htmlType="submit">
								{buttonTitle}
							</Button>
						</StyledItem>
					</Form>
				}
			/>
		</StyledCard>
	)
}

AdEditor.propTypes = {
	initialData: PropTypes.object,
	title: PropTypes.string.isRequired,
	onFinish: PropTypes.func.isRequired,
	finishLoading: PropTypes.bool.isRequired,
	buttonTitle: PropTypes.string.isRequired,
}
