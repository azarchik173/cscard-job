import { LOAD, DELETE, UPDATE } from './types'
import _ from 'loadsh'

export function rootReducer(state = { ads: [] }, action) {
	if (action.type === LOAD) {
		if (action.payload) {
			return { ads: action.payload }
		}
	} else if (action.type === DELETE) {
		if (action.payload) {
			const deleteId = action.payload.id
			return {
				ads: _.remove([...state.ads], (ad) => ad.id !== deleteId),
			}
		}
	} else if (action.type === UPDATE) {
		if (action.payload) {
			const updateId = action.payload.id
			const updatedAd = action.payload.adData
			return {
				ads: [...state.ads].map((el) => {
					if (el.id === updateId) {
						return updatedAd
					}
					return el
				}),
			}
		}
	}
	return state
}
