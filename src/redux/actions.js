import { LOAD, UPDATE, DELETE } from './types'

export function loadData(data) {
	return {
		type: LOAD,
		payload: data,
	}
}

export function updateAd(data) {
	return {
		type: UPDATE,
		payload: data,
	}
}

export function deleteAd(data) {
	return {
		type: DELETE,
		payload: data,
	}
}
