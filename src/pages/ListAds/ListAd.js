import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getAds, deleteAd as deleteApiAd } from '../../axios/interactionAPI'
import { loadData, deleteAd } from '../../redux/actions'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { Button, message } from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import _ from 'loadsh'

import Ad from '../../components/Ad'

const ListAdContainer = styled.article`
	display: flex;
	justify-content: center;
	flex-direction: column;
	max-width: 550px;
	margin: auto;
`
const ButtonStyled = styled(Button)`
	width: fit-content;
	place-self: flex-end;
	font-size: 11px !important;
`

const url = `${process.env.REACT_APP_URL}/ad`

export const ListAd = () => {
	const listAds = useSelector((state) => _.reverse([...state.ads]))
	const dispatch = useDispatch()
	const history = useHistory()

	useEffect(() => {
		dispatch((asyncDispatch) => {
			getAds(url)
				.then((response) => {
					asyncDispatch(loadData(response.data))
				})
				.catch((err) => {})
		})
	}, [dispatch])

	const onDelete = (id) => {
		deleteApiAd(url, id)
			.then(() => {
				dispatch(deleteAd({ id }))
				message.success('Объявление успешно удалено')
			})
			.catch(() => message.error('Произошла ошибка при удалении'))
	}
	return (
		<ListAdContainer>
			<ButtonStyled
				size="small"
				type="primary"
				shape="round"
				icon={<PlusOutlined />}
				onClick={() => history.push('/create-add')}
			>
				Добавить новое объявление
			</ButtonStyled>
			{listAds.map((ad) => (
				<Ad
					id={ad.id}
					key={ad.id}
					title={ad.title}
					description={ad.description}
					image={ad.image}
					onDelete={onDelete}
				/>
			))}
		</ListAdContainer>
	)
}
