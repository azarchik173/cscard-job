import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'

import { createAd } from '../../axios/interactionAPI'
import { AdEditor } from '../../components/AdEditor/AdEditor'
import { message } from 'antd'

const CreateAdContainer = styled.article`
	display: flex;
	justify-content: center;
	flex-direction: column;
	max-width: 550px;
	margin: auto;
`

const url = `${process.env.REACT_APP_URL}/ad`

export const CreateAd = () => {
	const history = useHistory()
	const [createLoading, updateLoading] = useState(false)
	useEffect(() => {
		if (history.action !== 'PUSH') {
			history.push('/')
		}
	}, [history])
	const create = ({ title, description, image }) => {
		updateLoading(true)
		createAd(url, {
			title,
			description,
			image,
		})
			.then(() => {
				message.success('Объявление успешно создано!')
				history.push('/')
			})
			.catch(() => {
				message.error('При создании объявления возникла ошибка')
			})
	}

	return (
		<CreateAdContainer>
			<AdEditor
				title="Создание нового объявления"
				onFinish={create}
				finishLoading={createLoading}
				buttonTitle="Создать"
			/>
		</CreateAdContainer>
	)
}
