import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { useParams, useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import _ from 'loadsh'

import AdEditor from '../../components/AdEditor'
import { updateAd } from '../../axios/interactionAPI'
import { updateAd as updateReduxAd } from '../../redux/actions'
import { message } from 'antd'

const EditAdContainer = styled.article`
	display: flex;
	justify-content: center;
	flex-direction: column;
	max-width: 550px;
	margin: auto;
`

const url = `${process.env.REACT_APP_URL}/ad`

export const EditAd = () => {
	let { id } = useParams()
	const history = useHistory()
	const currentAd = useSelector(
		(state) => state.ads[_.findIndex(state.ads, (ad) => ad.id === id)]
	)
	const dispatch = useDispatch()
	const [editedLoading, setEditedLoading] = useState(false)

	const editedFinish = ({ title, description, image }) => {
		setEditedLoading(true)
		dispatch((asyncDispatch) => {
			updateAd(url, id, {
				title,
				description,
				image,
			})
				.then((response) => {
					asyncDispatch(
						updateReduxAd({
							id,
							adData: response.data,
						})
					)
					message.success('Объявления успешно обновлено!')
					history.push('/')
				})
				.catch(() => message.error('При сохранении возникла ошибка'))
		})
	}

	useEffect(() => {
		if (history.action !== 'PUSH') {
			return history.push('/')
		}
	}, [history])

	return (
		<EditAdContainer>
			{currentAd && (
				<AdEditor
					title="Редактирование объявления"
					buttonTitle="Сохранить"
					onFinish={editedFinish}
					finishLoading={editedLoading}
					initialData={{
						title: currentAd.title,
						descrition: currentAd.description,
						image: currentAd.image,
					}}
				/>
			)}
		</EditAdContainer>
	)
}
